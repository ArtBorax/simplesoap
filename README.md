# SimpleSOAP  
Простой проект на Spring с SOAP

### Задание  
Реализовать на Java 1.8+ soap web-service, содержащий в себе метод Result findNumber(Integer number),  
ищущий в 20 разных по составу больших текстовых файлах (каждый должен быть порядка гб.) полученное на вход число n.  
Файлы состоят только из чисел, которые разделены между собой запятой.  
Результат работы необходимо записать в таблицу БД и вернуть объект Result в вызывающую систему.  

### Решение  
Класс FileTestData генерирует, проверяет и выполняет поиск значения в файлах.  
В качестве базы данных используется MySQL, а работа с базой выполняется через Hibernate. Таблица в базе создается автоматический по типу сущности Result.  
Класс ResultController реализует метод findNumber, где принимает значение через GET запрос в переменную number, ответ приходить в виде JSON  
Пример запроса: http://localhost:8887/findNumber?number=123  
Пример ответа: {"code":"00.Result.OK","fileNames":["16"],"error":""}  


### Config  
Настройки генерации файлов находятся в application.properties  
net.bor.simplesoap.filesize=1024 - размер одного файла в байтах  
net.bor.simplesoap.count=20 - количество файлов  
net.bor.simplesoap.checkfile=true - проверять файлы, работает когда createfile=false  
net.bor.simplesoap.createfile=true - создавать или перезаписывать файлы  
net.bor.simplesoap.folder=testData - путь или конечная папка, где находиться файлы  

