package net.bor.simplesoap.web;

public enum AnswerCode {
    OK("00.Result.OK"),
    NotFound("01.Result.NotFound"),
    Error("02.Result.Error");

    private final String code;

    AnswerCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
