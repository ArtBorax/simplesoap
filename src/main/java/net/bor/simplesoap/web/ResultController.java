package net.bor.simplesoap.web;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import net.bor.simplesoap.entity.Result;
import net.bor.simplesoap.service.FileTestData;
import net.bor.simplesoap.service.ResultSave;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResultController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final static String ERROR_INCORRECT_VALUE = "Incorrect value";
    private final static String ERROR_FILES_NULL = "Error get search files, files is NULL";
    private final static String ERROR_FILES_NOT_FOUND = "Search files not found";

    @Autowired
    private FileTestData fileTestData;

    @Autowired
    private ResultSave resultSave;

    @RequestMapping(value = "/findNumber", method = RequestMethod.GET)
    public ResulAnswer findNumber(@RequestParam(value = "number") String number) {
        ResulAnswer answer = new ResulAnswer();
        Result result = new Result();
        try {
            int num = Integer.parseInt(number);
            logger.info("Geted number:" + num);
            List<File> files = fileTestData.getFiles();
            if (files == null) {
                answer.setCode(AnswerCode.Error.getCode());
                answer.setError(ERROR_FILES_NULL);

                result.setCode(AnswerCode.Error.getCode());
                result.setError(ERROR_FILES_NULL);
                result.setNumber(num);
                resultSave.add(result);

                logger.error(ERROR_FILES_NULL);
                return answer;
            }
            if (files.isEmpty()) {
                answer.setCode(AnswerCode.Error.getCode());
                answer.setError(ERROR_FILES_NOT_FOUND);

                result.setCode(AnswerCode.Error.getCode());
                result.setError(ERROR_FILES_NOT_FOUND);
                result.setNumber(num);
                resultSave.add(result);

                logger.error(ERROR_FILES_NOT_FOUND);
                return answer;
            }
            List<String> foundFiles = files.stream().filter((file) -> (fileTestData.searchNumberInFile(file, num)))
                    .map(file -> file.getName())
                    .collect(Collectors.toList());
            answer.setFileNames(foundFiles);
            if (foundFiles.isEmpty()) {
                answer.setCode(AnswerCode.NotFound.getCode());

                result.setCode(AnswerCode.NotFound.getCode());
                result.setNumber(num);
                resultSave.add(result);

                logger.info("Geted number:" + num + " not found");
            } else {
                answer.setCode(AnswerCode.OK.getCode());

                result.setCode(AnswerCode.OK.getCode());
                result.setNumber(num);
                StringBuilder sb = new StringBuilder();
                for (String foundFile : foundFiles) {
                    if (sb.length() != 0) {
                        sb.append(",");
                    }
                    sb.append(foundFile);
                }
                result.setFileNames(sb.toString());
                resultSave.add(result);

                logger.info("Geted number:" + num + " found in files:" + sb.toString());
            }
        } catch (NumberFormatException e) {
            answer.setCode(AnswerCode.Error.getCode());
            answer.setError(ERROR_INCORRECT_VALUE);

            result.setCode(AnswerCode.Error.getCode());
            result.setError(ERROR_INCORRECT_VALUE);
            resultSave.add(result);

            logger.error("Incorrect value:\"" + number + "\"");
        }
        return answer;
    }

}
