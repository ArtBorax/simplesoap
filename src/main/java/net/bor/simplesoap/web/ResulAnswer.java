package net.bor.simplesoap.web;

import java.util.ArrayList;
import java.util.List;

public class ResulAnswer {

    public String code;
    public List<String> fileNames = new ArrayList<>();
    public String error = "";

    public void setCode(String code) {
        this.code = code;
    }

    public void setFileNames(List<String> fileNames) {
        this.fileNames = fileNames;
    }

    public void setError(String error) {
        this.error = error;
    }

}
