package net.bor.simplesoap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimplesoapApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimplesoapApplication.class, args);
    }

}
