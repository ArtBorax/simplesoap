package net.bor.simplesoap.repository;

import net.bor.simplesoap.entity.Result;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResultDB extends CrudRepository<Result, String> {

}
