package net.bor.simplesoap.service;

import net.bor.simplesoap.entity.Result;
import net.bor.simplesoap.repository.ResultDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ResultSave {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ResultDB resultDB;

    @Transactional
    public void add(Result result) {
        resultDB.save(result);
        logger.info("Add DB result");
    }

}
