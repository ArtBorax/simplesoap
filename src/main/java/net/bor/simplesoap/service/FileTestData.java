package net.bor.simplesoap.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StreamTokenizer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Создает файлы и заполняет их случайными числами в пределах значения {@code Integer} со знаком. Формат данных в файле {@code String} одной строкой,
 * значения разделены запятыми. Количество файлов, размер, расположение, генерация и проверка задается в {@code application.properties}. Проверка на
 * корректность содержимого файлов игнорируется при создании файлов. Если в указанной папке нет файлов, а создание файлов отключено, то будет запущенна
 * генерация файлов, а также если не существует и папок, то они будут созданы.
 *
 */
@Service
public class FileTestData {

    @Value("${net.bor.simplesoap.filesize}")
    private long filesize;

    @Value("${net.bor.simplesoap.count}")
    private int count;

    @Value("${net.bor.simplesoap.checkfile}")
    private boolean checkFile;

    @Value("${net.bor.simplesoap.createfile}")
    private boolean createFile;

    @Value("${net.bor.simplesoap.folder}")
    private String folderTestFile;

    private final Random random = new Random();
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final static String regex = "[0-9]+";
    private List<File> files;

    /**
     * Инициализирует переменные, создает файлы и папки, проверяет на корректность содержимого файлов если они не создаются согласно настройкам. Если в
     * указанной папке нет файлов, а создание файлов отключено, то будет запущенна генерация файлов, а также если не существует и папок, то они будут созданы и
     * заполнены файлами.
     */
    @PostConstruct
    public void init() {
        logger.info("Start initialization test data files");
        if (createFile) {
            files = createFiles(folderTestFile + File.separator, count, filesize);
        } else {
            files = getFiles(folderTestFile + File.separator);
            if (checkFile && files != null && !files.isEmpty()) {
                checkCorrectFiles(files);
            }
            if (files != null && !files.isEmpty()) {
                files = createFiles(folderTestFile + File.separator, count, filesize);
            }
        }
    }

    private void createFile(long size, File file) throws FileCreateException {
        if (size <= 0) {
            throw new FileCreateException("Error size file:" + size);
        }
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter(file));
        } catch (IOException ex) {
            logger.error(ex.toString());
            throw new FileCreateException("Error create file:" + file.getAbsolutePath());
        }
        int i = 0;
        for (; i < size;) {
            try {
                String s = random.nextInt() + ",";
                i = i + s.length();
                if (i < size) {
                    writer.write(s);
                } else {
                    writer.write(s.substring(0, s.length() - 1));
                    i--;
                }
            } catch (IOException ex) {
                logger.error(ex.toString());
                try {
                    if (writer != null) {
                        writer.close();
                    }
                } catch (IOException ex2) {
                    logger.error(ex2.toString());
                }
                throw new FileCreateException("Error write file:" + file.getAbsolutePath() + ". Write:" + i + " byte.");
            }
        }
        logger.info("Create file:" + file.getAbsolutePath() + " size:" + i);
        try {
            if (writer != null) {
                writer.close();
            }
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
    }

    /**
     * Создает полный путь, где будут созданы файлы. Создает файлы и заполняет их случайными значениями в пределах {@code Integer} со знаком. Имена файлов
     * состоят только из цифр и не имею расширения. Имя начинается с {@code 0} до {@code count-1}, если файл с таким именем уже существует, то он будет перезаписан.
     *
     * @param dir Директория или полный путь, где будут созданы файлы
     * @param count Количество создаваемых файлов
     * @param size Размер каждого файла в байтах
     * @return Список успешно созданных файлов, вернет пустой список если не удалось создать папки
     */
    public List<File> createFiles(String dir, int count, long size) {
        List<File> result = new ArrayList<>();
        File fdir = new File(dir);
        if (!fdir.exists() || !fdir.isDirectory()) {
            if (!fdir.mkdirs()) {
                logger.error("Error create folder");
                return result;
            }
        }
        for (int i = 0; i < count; i++) {
            File file = new File(dir + i);
            try {
                createFile(size, file);
                result.add(file);
            } catch (FileCreateException ex) {
                logger.error(ex.getMessage());
            }
        }
        return result;
    }

    /**
     * Возвращает список файлов созданных или найденных ранее, {@code null} если метод {@code init} не вызывался.
     *
     * @return Список файлов
     */
    public List<File> getFiles() {
        return files;
    }

    /**
     * Выполняет поиск в указанной директории файлов с именем только из цифр
     *
     * @param dir путь к файлам
     * @return список найденных файлов {@code null} если директории не существует
     */
    public List<File> getFiles(String dir) {
        File fdir = new File(dir);
        if (!fdir.exists() || !fdir.isDirectory()) {
            logger.error("Error, no folder:" + dir);
            return null;
        }
        return new ArrayList<>(Arrays.asList(fdir.listFiles((File pathname) -> pathname.getName().matches(regex) && pathname.isFile())));
    }

    /**
     * Выполняет проверку на содержимое в файлах, если файл не прошел проверку, то он удаляется из поданного списка в метод.
     *
     * @param files список файлов
     */
    public void checkCorrectFiles(List<File> files) {
        files.removeIf((File f) -> {
            if (f.length() == 0) {
                logger.error("Empty file: " + f.getAbsolutePath());
                return true;
            }
            Path file = Paths.get(f.getAbsolutePath());
            try ( BufferedReader in = Files.newBufferedReader(file)) {
                StreamTokenizer read = new StreamTokenizer(Files.newBufferedReader(file));
                while (read.nextToken() != StreamTokenizer.TT_EOF) {
                    if (read.ttype != StreamTokenizer.TT_NUMBER) {
                        logger.info("Incorrect file: " + f.getAbsolutePath());
                        return true;
                    }
                    read.nextToken();
                }
            } catch (IOException ex) {
                logger.error(ex.toString());
                return true;
            }
            return false;
        });
    }

    /**
     * Выполняет поиск значения в файле читая его последовательно
     *
     * @param file файл в котором будет произведен поиск
     * @param number искомое значение
     * @return возвращает {@code true} если в файле есть переданное значение, во всех остальных случаях возвращает {@code false}
     */
    public boolean searchNumberInFile(File file, int number) {
        Path pFile = Paths.get(file.getAbsolutePath());
        try ( BufferedReader in = Files.newBufferedReader(pFile)) {
            StreamTokenizer read = new StreamTokenizer(Files.newBufferedReader(pFile));
            while (read.nextToken() != StreamTokenizer.TT_EOF) {
                if (read.nval == number) {
                    logger.info("Found number in file: " + file.getAbsolutePath());
                    return true;
                }
                read.nextToken();
            }
        } catch (IOException ex) {
            logger.error(ex.toString());
        }
        return false;
    }

    private class FileCreateException extends Exception {

        public FileCreateException(String message) {
            super(message);
        }
    }
}
